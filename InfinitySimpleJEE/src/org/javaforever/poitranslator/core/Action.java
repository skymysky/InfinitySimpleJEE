package org.javaforever.poitranslator.core;

public class Action {
	protected String testCaseSerial;
	protected String testCaseDescription;
	protected String isIgnore;
	protected String result;
	protected String name;
	protected String xpath;
	protected String value;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getXpath() {
		return xpath;
	}
	public void setXpath(String xpath) {
		this.xpath = xpath;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getTestCaseSerial() {
		return testCaseSerial;
	}
	public void setTestCaseSerial(String testCaseSerial) {
		this.testCaseSerial = testCaseSerial;
	}
	public String getTestCaseDescription() {
		return testCaseDescription;
	}
	public void setTestCaseDescription(String testCaseDescription) {
		this.testCaseDescription = testCaseDescription;
	}
	public String getIsIgnore() {
		return isIgnore;
	}
	public void setIsIgnore(String isIgnore) {
		this.isIgnore = isIgnore;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	
	public boolean success(){
		if (!this.ignore() && this.name!=null && !this.name.equals("")&&this.name.startsWith("validate") && this.result!=null&&!this.result.equals("")&&this.result.equals("failure"))
			return false;
		else
			return true;
	}
	
	public boolean ignore(){
		if (this.isIgnore!=null&&!"".equals(this.isIgnore)&&this.isIgnore.equals("ignore"))return true;
		else return false;
	}
}
