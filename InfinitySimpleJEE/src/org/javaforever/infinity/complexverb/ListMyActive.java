package org.javaforever.infinity.complexverb;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.infinity.core.JQueryPositions;
import org.javaforever.infinity.core.Writeable;
import org.javaforever.infinity.domain.Domain;
import org.javaforever.infinity.domain.DragonHideStatement;
import org.javaforever.infinity.domain.JavascriptMethod;
import org.javaforever.infinity.domain.Method;
import org.javaforever.infinity.domain.Signature;
import org.javaforever.infinity.domain.Statement;
import org.javaforever.infinity.domain.StatementList;
import org.javaforever.infinity.domain.Type;
import org.javaforever.infinity.domain.Var;
import org.javaforever.infinity.generator.NamedStatementGenerator;
import org.javaforever.infinity.generator.NamedStatementListGenerator;
import org.javaforever.infinity.utils.InterVarUtil;
import org.javaforever.infinity.utils.SqlReflector;
import org.javaforever.infinity.utils.StringUtil;
import org.javaforever.infinity.utils.WriteableUtil;

public class ListMyActive extends TwoDomainVerb implements JQueryPositions{

	@Override
	public Method generateDaoImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));
		method.setReturnType(new Type("Set",this.slave, this.slave.getPackageToken()));
		method.addAdditionalImport("java.sql.Connection");
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport("java.util.ArrayList");
		method.addAdditionalImport("java.sql.PreparedStatement");
		method.addAdditionalImport("java.sql.ResultSet");
		method.addAdditionalImport("java.util.Set");
		method.addAdditionalImport("java.util.TreeSet");
		method.addAdditionalImport(this.master.getPackageToken()+".domain."+this.master.getStandardName());
		method.addAdditionalImport(this.slave.getPackageToken()+".domain."+this.slave.getStandardName());
		method.addSignature(new Signature(1,InterVarUtil.DB.connection.getVarName(),InterVarUtil.DB.connection.getVarType()));
		method.addSignature(new Signature(2,this.master.getLowerFirstDomainName()+"Id",this.master.getDomainId().getClassType()));
		method.setThrowException(true);
		
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(100L,2,"String "+InterVarUtil.DB.query.getVarName() +" = \"" +SqlReflector.generateSelectActiveUsingMasterIdStatement(master,slave)+";\";"));
		list.add(NamedStatementGenerator.getPrepareStatement(200L,2,InterVarUtil.DB.ps, InterVarUtil.DB.query, InterVarUtil.DB.connection));		
		list.add(new Statement(300L,2,InterVarUtil.DB.ps.getVarName()+"."+this.master.getDomainId().getClassType().getTypeSetterName()+"(1,"+this.master.getLowerFirstDomainName()+"Id);"));
		list.add(NamedStatementGenerator.getPrepareStatementExcuteQuery(400L,2, InterVarUtil.DB.result, InterVarUtil.DB.ps));
		list.add(NamedStatementGenerator.getPrepareDaomainListUsingArrayList(500L,2, slave));
		list.add(NamedStatementGenerator.getResultSetWhileNextLoopHead(600L,2, InterVarUtil.DB.result));  
		list.add(NamedStatementGenerator.getSingleLineComment(700L,3,"Build the list object."));
		list.add(NamedStatementGenerator.getPrepareDomainVarInit(800L,3, slave));
		list.add(NamedStatementListGenerator.generateSetDomainDataFromResultSet(900L,3,slave, InterVarUtil.DB.result));
		list.add(NamedStatementGenerator.getSingleLineComment(1000L,3, "Build the object list."));			
		list.add(NamedStatementGenerator.getAddDomaintoList(1100L,3, slave, InterVarUtil.Container.getList(slave)));
		list.add(NamedStatementGenerator.getLoopFooter(1200L,2));
		list.add(new Statement(1300L,2,"Set<"+slave.getCapFirstDomainName()+"> retVals = new TreeSet<"+slave.getCapFirstDomainName()+">();"));
		list.add(new Statement(1400L,2, "retVals.addAll(list);"));
		list.add(new Statement(1500L,2, "return retVals;"));
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}
	
	@Override
	public String generateDaoImplMethodString() throws Exception {
		Method m = this.generateDaoImplMethod();
		String s = m.generateMethodString();
		return s;
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		Method m = this.generateDaoImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));
		method.setReturnType(new Type("Set",this.slave, this.slave.getPackageToken()));
		method.addAdditionalImport("java.util.Set");
		method.addAdditionalImport(this.slave.getPackageToken()+".domain."+this.slave.getStandardName());
		method.addSignature(new Signature(1,InterVarUtil.DB.connection.getVarName(),InterVarUtil.DB.connection.getVarType()));
		method.addSignature(new Signature(2,this.master.getLowerFirstDomainName()+"Id",this.master.getDomainId().getClassType()));
		method.setThrowException(true);
		return method;
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		return generateDaoMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));
		method.setReturnType(new Type("Set",this.slave, this.slave.getPackageToken()));
		method.addAdditionalImport("java.util.Set");
		method.addAdditionalImport(this.slave.getPackageToken()+".domain."+this.slave.getStandardName());
		method.addSignature(new Signature(1,this.master.getLowerFirstDomainName()+"Id", this.master.getDomainId().getClassType()));
		method.setThrowException(true);
		return method;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));		
		method.setReturnType(new Type("Set",this.slave, this.slave.getPackageToken()));
		method.addSignature(new Signature(1,this.master.getLowerFirstDomainName()+"Id", this.master.getDomainId().getClassType()));
		method.addAdditionalImport("java.util.Set");
		method.addAdditionalImport("java.util.TreeSet");
		method.addAdditionalImport(this.slave.getPackageToken()+".domain."+this.slave.getStandardName());
		method.addAdditionalImport(this.master.getPackageToken()+".dao."+this.master.getStandardName()+"Dao");
		method.addAdditionalImport(this.slave.getPackageToken()+".service."+this.slave.getStandardName()+"Service");
		method.setThrowException(true);
		method.addMetaData("Override");
		
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(NamedStatementGenerator.getDBConfInitDBAutoRelease(1000L, 2, InterVarUtil.DB.connection, InterVarUtil.DB.dbconf));
		list.add(new Statement(2000L,3,"Set<"+this.slave.getCapFirstDomainName()+"> set = new TreeSet<"+this.slave.getStandardName()+">();"));
		list.add(new Statement(3000L,3,"set.addAll(dao."+StringUtil.lowerFirst(this.getVerbName())+"("+InterVarUtil.DB.connection.getVarName()+","+this.master.getLowerFirstDomainName()+"Id"+"));"));
		list.add(new Statement(4000L,3,"return set;"));
		list.add(new Statement(5000L,2,"}"));
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception {
		Method m = this.generateServiceImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public Method generateFacadeMethod() throws Exception {
		Method method = new Method();
		method.setStandardName("processRequest");
		method.setReturnType(new Type("void"));
		method.setThrowException(true);
		List<String> list = new ArrayList<String>();
		list.add("ServletException");
		list.add("IOException");
		method.setIsprotected(true);
		method.setOtherExceptions(list);
		method.addSignature(new Signature(1,"request",new Type("HttpServletRequest","javax.servlet.http")));
		method.addSignature(new Signature(2,"response",new Type("HttpServletResponse","javax.servlet.http")));
		method.addAdditionalImport("java.io.IOException");
		method.addAdditionalImport("java.io.PrintWriter");
		method.addAdditionalImport("javax.servlet.ServletException");
		method.addAdditionalImport("javax.servlet.http.HttpServlet");
		method.addAdditionalImport("javax.servlet.http.HttpServletRequest");
		method.addAdditionalImport("javax.servlet.http.HttpServletResponse");
		method.addAdditionalImport("java.util.Map");
		method.addAdditionalImport("java.util.TreeMap");
		method.addAdditionalImport("java.util.Set");
		method.addAdditionalImport("java.util.TreeSet");
		method.addAdditionalImport("net.sf.json.JSONObject");
		method.addAdditionalImport(this.master.getPackageToken()+".utils.StringUtil");
		method.addAdditionalImport(this.slave.getPackageToken()+".domain."+this.slave.getStandardName());
		method.addAdditionalImport(this.master.getPackageToken()+".service."+this.master.getStandardName()+"Service");
		method.addAdditionalImport(this.master.getPackageToken()+".serviceimpl."+this.master.getStandardName()+"ServiceImpl");
		
		List<Writeable> wlist = new ArrayList<Writeable>();
		Var service = new Var("service", new Type(this.master.getStandardName()+"Service",this.master.getPackageToken()));
		Var resultMap = new Var("result", new Type("TreeMap<String,Object>","java.util"));
		
		wlist.add(NamedStatementGenerator.getFacadeSetContentType(1000L, 2, InterVarUtil.Servlet.response, InterVarUtil.SimpleJEE.UTF8.getVarName()));
		wlist.add(NamedStatementGenerator.getControllerPrintWriterOut(2000L, 2, InterVarUtil.Servlet.response, InterVarUtil.Servlet.out));
		wlist.add(NamedStatementGenerator.getJsonResultMap(3000L, 2, resultMap));
		wlist.add(new Statement(4000L,2, service.getVarType() +" " +service.getVarName() +" = new " +service.getVarType().getClassType()+"Impl();"));
		wlist.add(NamedStatementGenerator.getTryHead(5000L, 2));
		wlist.add(new Statement(6000L,3,this.master.getDomainId().getFieldRawType().getClassType()+ " " + this.master.getLowerFirstDomainName()+"Id = "+Type.getInitTypeWithVal(this.master.getDomainId().getClassType())+";"));
		wlist.add(new DragonHideStatement(8000L,3,"if (!StringUtil.isBlank(request.getParameter(\""+this.master.getLowerFirstDomainName()+"Id\"))) "+this.master.getLowerFirstDomainName()+"Id = Long.parseLong(request.getParameter(\""+this.master.getLowerFirstDomainName()+"Id\"));",this.master.getDomainId().getFieldType().equalsIgnoreCase("long")));
		wlist.add(new DragonHideStatement(8000L,3,"if (!StringUtil.isBlank(request.getParameter(\""+this.master.getLowerFirstDomainName()+"Id\"))) "+this.master.getLowerFirstDomainName()+"Id = Integer.parseInt(request.getParameter(\""+this.master.getLowerFirstDomainName()+"Id\"));",this.master.getDomainId().getFieldType().equals("int")||this.master.getDomainId().getFieldType().equals("Integer")));
		wlist.add(new Statement(8000L,3,"Set<"+this.slave.getCapFirstDomainName()+"> set = new TreeSet<"+this.slave.getCapFirstDomainName()+">();"));
		wlist.add(new Statement(10000L,3,"if ("+this.master.getLowerFirstDomainName()+"Id != null) set = "+service.getVarName()+"."+this.generateServiceMethodDefinition().getStandardCallString()+";"));
		wlist.add(new Statement(11000L,3, resultMap.getVarName()+".put(\"success\",true);"));
		wlist.add(new Statement(12000L,3, resultMap.getVarName()+".put(\"data\",set);"));
		wlist.add(NamedStatementGenerator.getEncodeMapToJsonResultMap(13000L, 3, resultMap,InterVarUtil.Servlet.out));
		wlist.add(NamedStatementListGenerator.generateCatchExceptionPrintStackPrintJsonMapFinallyCloseOutFooter(14000L, 2, InterVarUtil.Servlet.response, resultMap, InterVarUtil.Servlet.out));
		method.setMethodStatementList(WriteableUtil.merge(wlist));		
		return method;
	}
	
	@Override
	public String generateFacadeMethodString() throws Exception {
		Method m = this.generateFacadeMethod();
		return m.generateMethodString();
	}

	@Override
	public String generateFacadeMethodStringWithSerial() throws Exception {
		Method m = this.generateFacadeMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	public ListMyActive(Domain master,Domain slave){
		super();
		this.master = master;
		this.slave = slave;
		this.setVerbName("ListActive"+this.master.getCapFirstDomainName()+this.slave.getCapFirstPlural()+"Using"+this.master.getCapFirstDomainName()+"Id");
		this.setLabel("列出所属");
	}

	@Override
	public JavascriptMethod generateJQueryActionMethod() throws Exception {
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));
		
		StatementList sl = new StatementList();
		sl.add(new Statement(1000,1, "var "+this.master.getLowerFirstDomainName()+"Id = $(\"select#my"+this.master.getCapFirstDomainName()+">option:nth-child(\"+(currentIndex+1)+\")\").val();"));
		sl.add(new Statement(2000,1, "$.ajax({"));
		sl.add(new Statement(3000,2, "type: \"post\","));
		sl.add(new Statement(4000,2, "url: \"../facade/"+StringUtil.lowerFirst(this.getVerbName())+"Facade\","));
		sl.add(new Statement(5000,2, "dataType: 'json',"));
		sl.add(new Statement(6000,2, "data :{"+this.master.getLowerFirstDomainName()+"Id:"+this.master.getLowerFirstDomainName()+"Id},"));
		sl.add(new Statement(7000,2, "success: function(data, textStatus) {"));
		sl.add(new Statement(8000,3, "if (data.success) {"));
		sl.add(new Statement(9000,4, "$(\"#my"+this.slave.getCapFirstPlural()+"\").html(\"\");"));
		sl.add(new Statement(10000,4, "generate"+this.slave.getCapFirstDomainName()+"Options(data.data,\"my"+this.slave.getCapFirstPlural()+"\");"));
		sl.add(new Statement(11000,3, "}"));
		sl.add(new Statement(12000,2, "},"));
		sl.add(new Statement(13000,2, "complete : function(XMLHttpRequest, textStatus) {"));
		sl.add(new Statement(14000,2, "},"));
		sl.add(new Statement(15000,2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sl.add(new Statement(16000,3, "alert(\"Error:\"+textStatus);"));
		sl.add(new Statement(17000,3, "alert(errorThrown.toString());"));
		sl.add(new Statement(18000,2, "}"));
		sl.add(new Statement(19000,1, "});"));
		
		method.setMethodStatementList(sl);
		return method;	
	}
	
	@Override
	public String generateJQueryActionString() throws Exception {
		return generateJQueryActionMethod().generateMethodString();
	}

	@Override
	public String generateJQueryActionStringWithSerial() throws Exception {
		return generateJQueryActionMethod().generateMethodStringWithSerial();
	}

}
